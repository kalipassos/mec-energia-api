import pytest
from rest_framework import status
from rest_framework.test import APIClient

from tests.test_utils import create_objects_test_utils, dicts_test_utils
from universities.models import ConsumerUnit

ENDPOINT = '/api/recommendation/'

@pytest.mark.django_db
class TestConsumerUnitsEndpoint:
    def setup_method(self):
        self.university_dict = dicts_test_utils.university_dict_1
        self.user_dict = dicts_test_utils.university_user_dict_1

        self.university = create_objects_test_utils.create_test_university(self.university_dict)
        self.user = create_objects_test_utils.create_test_university_user(self.user_dict, self.university)
        
        self.client = APIClient()
        self.client.login(email = self.user_dict['email'], password = self.user_dict['password'])
        
        self.consumer_unit_test_1_dict = dicts_test_utils.consumer_unit_dict_1
        self.consumer_unit_test_1 = create_objects_test_utils.create_test_consumer_unit(self.consumer_unit_test_1_dict, self.university)   

        self.distributor_test_1_dict = dicts_test_utils.distributor_dict_1
        self.distributor_test_1 = create_objects_test_utils.create_test_distributor(self.distributor_test_1_dict, self.university)

        self.contract_test_1_dict = dicts_test_utils.contract_dict_1
        self.contract_test_1 = create_objects_test_utils.create_test_contract(self.contract_test_1_dict,  self.distributor_test_1, self.consumer_unit_test_1)
    
    
    def test_retrieve__with_nonexistent_consumer_unit__should_return_404(self):
        # Arrange
        error_response = {'errors': ['Consumer unit does not exist']}
        
        # Act
        response = self.client.get(f'{ENDPOINT}{self.consumer_unit_test_1.id + 1}/')
        
        # Assert
        assert response.status_code == status.HTTP_404_NOT_FOUND
        assert response.data == error_response
        
    def test_retrieve__with_inactive_consumer_unit__should_return_400(self):
        # Arrange
        consumer_unit = ConsumerUnit.objects.get(pk=self.consumer_unit_test_1.id)
        consumer_unit.is_active = False
        consumer_unit.save()
        
        error_response = {'errors': ['Consumer unit is not active']}
        
        # Act
        response = self.client.get(f'{ENDPOINT}{self.consumer_unit_test_1.id}/')
        
        # Assert
        assert response.status_code == status.HTTP_400_BAD_REQUEST
        assert response.data == error_response
        
    def test_retrieve__with_no_tariffs__should_return_200(self):
        # Arrange
        # Act
        response = self.client.get(f'{ENDPOINT}{self.consumer_unit_test_1.id}/')
        
        # Assert
        assert response.status_code == status.HTTP_200_OK
        
    def test_retrieve__with_a_expired_tariff__should_return_200(self):
        # Arrange
        tariff_dict = dicts_test_utils.tariff_dict_2
        create_objects_test_utils.create_test_tariffs(tariff_dict, self.distributor_test_1)
        
        # Act
        response = self.client.get(f'{ENDPOINT}{self.consumer_unit_test_1.id}/')
        
        # Assert
        assert response.status_code == status.HTTP_200_OK
        assert 'Atualize as tarifas vencidas para aumentar a precisão da análise' in response.data['warnings']
        
    def test_retrieve__with_a_non_expired_tariff__should_return_200(self):
        # Arrange
        tariff_dict = dicts_test_utils.tariff_dict_3
        create_objects_test_utils.create_test_tariffs(tariff_dict, self.distributor_test_1)
        
        # Act
        response = self.client.get(f'{ENDPOINT}{self.consumer_unit_test_1.id}/')
        
        # Assert
        assert response.status_code == status.HTTP_200_OK
        assert 'Atualize as tarifas vencidas para aumentar a precisão da análise' not in response.data['warnings']